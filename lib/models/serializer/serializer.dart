library serializer;

import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';

import 'package:built_collection/built_collection.dart';
import 'package:loginwithfb/models/user_model/user_model.dart';

part 'serializer.g.dart';

@SerializersFor(const [
  UserModel,
])
final Serializers serializers =
    (_$serializers.toBuilder()..addPlugin(StandardJsonPlugin())).build();
