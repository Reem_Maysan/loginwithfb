library user_model;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:loginwithfb/models/serializer/serializer.dart';

part 'user_model.g.dart';

abstract class UserModel implements Built<UserModel, UserModelBuilder> {
  // fields go here

  UserModel._();

  int get id;

  @nullable
  String get name;

  @nullable
  String get email;

  @nullable
  @BuiltValueField(wireName: 'user_country')
  String get country;

  @nullable
  @BuiltValueField(wireName: 'email_verified_at')
  String get verified;

  @nullable
  int get approve;
  @nullable
  String get token;

  @nullable
  String get avatar;

  
  factory UserModel([updates(UserModelBuilder b)]) = _$UserModel;

  String toJson() {
    return json.encode(serializers.serializeWith(UserModel.serializer, this));
  }

  static UserModel fromJson(String jsonString) {
    return serializers.deserializeWith(
        UserModel.serializer, json.decode(jsonString));
  }

  static Serializer<UserModel> get serializer => _$userModelSerializer;
}
