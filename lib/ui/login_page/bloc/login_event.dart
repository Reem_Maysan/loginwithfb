library login_event;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';

part 'login_event.g.dart';

abstract class LoginEvent {}

abstract class TryLogin extends LoginEvent
    implements Built<TryLogin, TryLoginBuilder> {
  // fields go here

  String get email;

  String get password;

  TryLogin._();

  factory TryLogin([updates(TryLoginBuilder b)]) = _$TryLogin;
}

abstract class ClearError extends LoginEvent
    implements Built<ClearError, ClearErrorBuilder> {
  // fields go here

  ClearError._();

  factory ClearError([updates(ClearErrorBuilder b)]) = _$ClearError;
}

abstract class FaceBookLogin extends LoginEvent
    implements Built<FaceBookLogin, FaceBookLoginBuilder> {
  // fields go here

  String get email;

  String get name;

  String get token;

  int get id;

  String get image;
  FaceBookLogin._();

  factory FaceBookLogin([updates(FaceBookLoginBuilder b)]) = _$FaceBookLogin;
}
