// GENERATED CODE - DO NOT MODIFY BY HAND

part of login_event;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

class _$TryLogin extends TryLogin {
  @override
  final String email;
  @override
  final String password;

  factory _$TryLogin([void Function(TryLoginBuilder) updates]) =>
      (new TryLoginBuilder()..update(updates)).build();

  _$TryLogin._({this.email, this.password}) : super._() {
    if (email == null) {
      throw new BuiltValueNullFieldError('TryLogin', 'email');
    }
    if (password == null) {
      throw new BuiltValueNullFieldError('TryLogin', 'password');
    }
  }

  @override
  TryLogin rebuild(void Function(TryLoginBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  TryLoginBuilder toBuilder() => new TryLoginBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is TryLogin &&
        email == other.email &&
        password == other.password;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, email.hashCode), password.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('TryLogin')
          ..add('email', email)
          ..add('password', password))
        .toString();
  }
}

class TryLoginBuilder implements Builder<TryLogin, TryLoginBuilder> {
  _$TryLogin _$v;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _password;
  String get password => _$this._password;
  set password(String password) => _$this._password = password;

  TryLoginBuilder();

  TryLoginBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _password = _$v.password;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(TryLogin other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$TryLogin;
  }

  @override
  void update(void Function(TryLoginBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$TryLogin build() {
    final _$result = _$v ?? new _$TryLogin._(email: email, password: password);
    replace(_$result);
    return _$result;
  }
}

class _$ClearError extends ClearError {
  factory _$ClearError([void Function(ClearErrorBuilder) updates]) =>
      (new ClearErrorBuilder()..update(updates)).build();

  _$ClearError._() : super._();

  @override
  ClearError rebuild(void Function(ClearErrorBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ClearErrorBuilder toBuilder() => new ClearErrorBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ClearError;
  }

  @override
  int get hashCode {
    return 507656265;
  }

  @override
  String toString() {
    return newBuiltValueToStringHelper('ClearError').toString();
  }
}

class ClearErrorBuilder implements Builder<ClearError, ClearErrorBuilder> {
  _$ClearError _$v;

  ClearErrorBuilder();

  @override
  void replace(ClearError other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ClearError;
  }

  @override
  void update(void Function(ClearErrorBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ClearError build() {
    final _$result = _$v ?? new _$ClearError._();
    replace(_$result);
    return _$result;
  }
}

class _$FaceBookLogin extends FaceBookLogin {
  @override
  final String email;
  @override
  final String name;
  @override
  final String token;
  @override
  final int id;
  @override
  final String image;

  factory _$FaceBookLogin([void Function(FaceBookLoginBuilder) updates]) =>
      (new FaceBookLoginBuilder()..update(updates)).build();

  _$FaceBookLogin._({this.email, this.name, this.token, this.id, this.image})
      : super._() {
    if (email == null) {
      throw new BuiltValueNullFieldError('FaceBookLogin', 'email');
    }
    if (name == null) {
      throw new BuiltValueNullFieldError('FaceBookLogin', 'name');
    }
    if (token == null) {
      throw new BuiltValueNullFieldError('FaceBookLogin', 'token');
    }
    if (id == null) {
      throw new BuiltValueNullFieldError('FaceBookLogin', 'id');
    }
    if (image == null) {
      throw new BuiltValueNullFieldError('FaceBookLogin', 'image');
    }
  }

  @override
  FaceBookLogin rebuild(void Function(FaceBookLoginBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  FaceBookLoginBuilder toBuilder() => new FaceBookLoginBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is FaceBookLogin &&
        email == other.email &&
        name == other.name &&
        token == other.token &&
        id == other.id &&
        image == other.image;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc($jc(0, email.hashCode), name.hashCode), token.hashCode),
            id.hashCode),
        image.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('FaceBookLogin')
          ..add('email', email)
          ..add('name', name)
          ..add('token', token)
          ..add('id', id)
          ..add('image', image))
        .toString();
  }
}

class FaceBookLoginBuilder
    implements Builder<FaceBookLogin, FaceBookLoginBuilder> {
  _$FaceBookLogin _$v;

  String _email;
  String get email => _$this._email;
  set email(String email) => _$this._email = email;

  String _name;
  String get name => _$this._name;
  set name(String name) => _$this._name = name;

  String _token;
  String get token => _$this._token;
  set token(String token) => _$this._token = token;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _image;
  String get image => _$this._image;
  set image(String image) => _$this._image = image;

  FaceBookLoginBuilder();

  FaceBookLoginBuilder get _$this {
    if (_$v != null) {
      _email = _$v.email;
      _name = _$v.name;
      _token = _$v.token;
      _id = _$v.id;
      _image = _$v.image;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(FaceBookLogin other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$FaceBookLogin;
  }

  @override
  void update(void Function(FaceBookLoginBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$FaceBookLogin build() {
    final _$result = _$v ??
        new _$FaceBookLogin._(
            email: email, name: name, token: token, id: id, image: image);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
