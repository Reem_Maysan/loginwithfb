
import 'package:bloc/bloc.dart';
import 'package:loginwithfb/data/repository/irepository.dart';

import 'login_event.dart';
import 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  IRepository _repository;

  LoginBloc(this._repository);

  @override
  LoginState get initialState => LoginState.initail();

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is ClearError) {
      yield state.rebuild((b) => b..error = "");
    }
    if (event is FaceBookLogin) {
      try {
        yield state.rebuild((b) => b
          ..isLoading = true
          ..error = ""
          ..success = false);

        final date = await _repository.facebookLogin(
            email: event.email, name: event.name, token: event.token,id: event.id,image: event.image);
        print('FaceBookLogin Success data ${date}');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = ""
          ..success = true);
      } catch (e) {
        print('FaceBookLogin Error $e');
        yield state.rebuild((b) => b
          ..isLoading = false
          ..error = "Something went wrong"
          ..success = false);
      }
    }
  }
}
