import 'package:flutter/material.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as JSON;

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final facebookLogin = FacebookLogin();
  bool _isLoggedIn = false;

  Map userProfile;

  _Login() async {
    final result = await facebookLogin.logIn(['email']);
    //logInWithReadPermissions(['email']);

    switch (result.status) {
      case FacebookLoginStatus.loggedIn:
        final token = result.accessToken.token;
        final graphResponse = await http.get(

            'https://graph.facebook.com/v2.12/me?fields=name,picture,email&access_token=$token');
        final profile = JSON.jsonDecode(graphResponse.body);
        print('--------------------------------------');

        print(profile);
        print('--------------------------------------');

        setState(() {
          userProfile = profile;
          _isLoggedIn = true;
          print('************************************************');
          print('userProfile name: ');
          print(userProfile['name']);
          print('userProfile email: ');
          print(userProfile['email']);
          print('userProfile last_name: ');
          print(userProfile['last_name']);
          print('************************************************');
        });
        break;

      case FacebookLoginStatus.cancelledByUser:
        setState(() => _isLoggedIn = false);
        break;
      case FacebookLoginStatus.error:
        setState(() => _isLoggedIn = false);
        break;
    }
  }

  _logout() {
    facebookLogin.logOut();
    setState(() {
      _isLoggedIn = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.blue,
      child: _isLoggedIn
          ? Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.network(
                  userProfile["picture"]["data"]["url"],
                  height: 50.0,
                  width: 50.0,
                ),
                Text(userProfile["name"]),
                OutlineButton(
                  child: Text('Logout'),
                  onPressed: () {
                    _logout();
                  },
                ),
              ],
            )
          : Center(
              child: OutlineButton(
                child: Text('Login with FaceBook'),
                onPressed: () {
                  _Login();
                },
              ),
            ),
    );
  }
}
