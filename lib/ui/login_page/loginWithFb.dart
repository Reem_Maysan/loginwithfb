import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:loginwithfb/core/app_localizations.dart';
import 'package:loginwithfb/core/constent.dart';
import 'package:loginwithfb/core/custom_loader.dart';
import 'package:loginwithfb/injectoin.dart';
import 'package:loginwithfb/ui/home_page.dart';
import 'package:loginwithfb/ui/login_page/bloc/login_bloc.dart';
import 'package:loginwithfb/ui/login_page/bloc/login_event.dart';
import 'package:loginwithfb/ui/login_page/bloc/login_state.dart';
import 'dart:convert' as JSON;
import 'dart:io';

//import com.facebook.FacebookSdk;
//import com.facebook.appevents.AppEventsLogger;

import 'package:page_transition/page_transition.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final facebookLogin = FacebookLogin();
  final _bloc = sl<LoginBloc>();

  var _emailController = TextEditingController();
  var _passwordController = TextEditingController();

  Future<bool> _onWillPop() async {
    exit(0);
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _bloc,
        builder: (BuildContext context, LoginState state) {
          error(state.error);
          if (state.success) {
            WidgetsBinding.instance.addPostFrameCallback((_) =>
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => Home())));
          }
          return WillPopScope(
            onWillPop: _onWillPop,
            child: Scaffold(
              backgroundColor: AppColor.background,
              body: Stack(
                children: <Widget>[
                  SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsetsDirectional.only(
                              start: 15.0, end: 15.0, top: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                width: 60,
                                height: 1,
                                color: Colors.red,
                              ),
                            ],
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            print('facebookClick');
                            final result = await facebookLogin.logIn([
                              'email',
                            ]);
                            //   .logInWithReadPermissions(['email',]);
                            switch (result.status) {
                              case FacebookLoginStatus.loggedIn:
                                {
                                  final token = result.accessToken.token;
                                  final graphResponse = await http.get(
                                      'https://graph.facebook.com/v2.12/me?fields=name,picture.width(800).height(800),first_name,last_name,email&access_token=$token');
                                  final profile =
                                      JSON.jsonDecode(graphResponse.body);
                                  if (profile != null) {
                                    if (profile["name"].toString().isNotEmpty) {
                                      _bloc.add(FaceBookLogin((b) => b
                                        ..email =
                                            profile["email"] ?? profile["id"]
                                        ..name = profile["name"]
                                        ..image =
                                            profile["picture"]["data"]["url"]
                                        ..token = token
                                        ..id = int.parse(profile["id"])));
                                    }
                                  }
                                }
                                break;
                              case FacebookLoginStatus.cancelledByUser:
                                //_showCancelledMessage();
                                break;
                              case FacebookLoginStatus.error:
                                print(
                                    'FacebookLoginStatus Error : ${result.errorMessage}');
                                break;
                            }
                          },
                          child: Padding(
                            padding: EdgeInsetsDirectional.only(
                                start: 15.0, end: 15.0, top: 40),
                            child: Container(
                              height: 40,
                              width: 170,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image: AssetImage(
                                          "assets/image/facebook_login.png"),
                                      fit: BoxFit.fill)),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  state.isLoading
                      ? Center(child: loader(context: context))
                      : Container()
                ],
              ),
            ),
          );
        });
  }

  void error(String errorCode) {
    if (errorCode.isNotEmpty) {
      Fluttertoast.showToast(
          msg: errorCode,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          backgroundColor: AppColor.orangeColor,
          textColor: Colors.white,
          fontSize: 16.0);
      _bloc.add(ClearError());
    }
  }
}
