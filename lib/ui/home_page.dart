import 'package:flutter/material.dart';

class Home extends StatefulWidget {
   int page;
  bool isLogin ;

  Home({this.page=0,this.isLogin = true});
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('HomePage'),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.blueGrey,
        child: Center(child: Text('Hello from home page!!'),),
      ),
    );
  }
}