import 'package:flutter/material.dart';
import 'package:loginwithfb/injectoin.dart';
import 'package:loginwithfb/ui/login_page/loginWithFb.dart';

void main()  async {
  await iniGetIt();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: LoginPage(),
    );
  }
}
