import 'dart:async';
import 'package:floor/floor.dart';
import 'package:loginwithfb/data/db_helper/dao/typee_dao.dart';
import 'package:loginwithfb/data/db_helper/entites/typee.dart';

import 'package:sqflite/sqflite.dart' as sqflite;

part 'database.g.dart'; // the generated code will be there

@Database(version: 1, entities: [Typee])
abstract class AppDatabase extends FloorDatabase {
  /*
In order to retrieve the PersonDao instance,
invoking the persoDao getter on the database instance is enough.
Its functions can be used as shown in the following snippet.
  */
  TypeeDao get typeeDao;
}
