import 'db/database.dart';
import 'entites/typee.dart';

abstract class IDbHelper {
  
  Future<AppDatabase> _getInstDB() {}


  /// Typee
  Future<void> insertTypee(Typee type);

  Future<List<Typee>> getTypee();

  Future<void> deleteTypee(int id);
}
