

import 'Idb_helper.dart';
import 'db/database.dart';
import 'entites/typee.dart';

/*
Use the generated code. For obtaining an instance of the database, 
use the generated $FloorAppDatabase class, which allows access to a database builder.
The name is being composed by $Floor and the database class name.
The string passed to databaseBuilder() will be the database file name.
For initializing the database, call build() and make sure to await the result.
*/

class DbHelper implements IDbHelper {
  @override
  Future<AppDatabase> _getInstDB() async {
    return await $FloorAppDatabase.databaseBuilder('appDB.db').build();
  }

  @override
  Future<void> deleteTypee(int id) async {
    try {
      return await (await _getInstDB()).typeeDao.deleteTypee(id);
    } catch (e) {
      print('wishList db e is $e');
      throw Exception(e.toString());
    }
  }

  @override
  Future<List<Typee>> getTypee() async {
    try {
      return await (await _getInstDB()).typeeDao.getTypee();
    } catch (e) {
      print('wishList db e is $e');
      throw Exception(e.toString());
    }
  }

  @override
  Future<void> insertTypee(Typee type) async {
    try {
      return await (await _getInstDB()).typeeDao.insertTypee(type);
    } catch (e) {
      print('wishList db e is $e');
      throw Exception(e.toString());
    }
  }
}
