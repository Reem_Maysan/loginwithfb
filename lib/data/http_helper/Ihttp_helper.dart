import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:loginwithfb/models/user_model/user_model.dart';

abstract class IHttpHelper {

  Future<UserModel> facebookLogin({String email, String name,String token,id,String image});

  Future<bool> logout();
}
