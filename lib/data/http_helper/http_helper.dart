import 'dart:convert';
import 'dart:io';

import 'package:built_collection/src/list.dart';
import 'package:built_value/serializer.dart';

import 'package:dio/dio.dart';
import 'package:dio_cookie_manager/dio_cookie_manager.dart';
import 'package:loginwithfb/core/error.dart';
import 'package:loginwithfb/models/serializer/serializer.dart';
import 'package:loginwithfb/models/user_model/user_model.dart';

import 'Ihttp_helper.dart';
import 'dart:io';
import 'package:path/path.dart';
import 'package:cookie_jar/cookie_jar.dart';

class HttpHelper implements IHttpHelper {
  final Dio _dio;

  var cookieJar = CookieJar();

  HttpHelper(this._dio) {
    _dio.interceptors.add(
      LogInterceptor(
        request: true,
        responseBody: true,
        requestBody: true,
      ),
    );
    _dio.interceptors.add(CookieManager(cookieJar));
  }

  @override
  Future<bool> logout() async {
    _dio.interceptors.add(CookieManager(cookieJar));
    try {
      final response = await _dio.post('logout');
      print('logout Response StatusCode ${response.statusCode}');
      if (response.statusCode == 200) {
        return true;
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e.toString());
      throw NetworkException();
    }
  }

  @override
  Future<UserModel> facebookLogin(
      {String email, String name, String token, id, String image}) async {
    try {
      final formData = {
        "email": email,
        "name": name,
        "access_token": token,
        "id": id,
        "avatar": image
      };
      _dio.interceptors.add(CookieManager(cookieJar));
      final response = await _dio.post('login/facebook', data: formData);
      print('facebookLogin Response StatusCode ${response.statusCode}');

      if (response.statusCode == 200) {
        final UserModel baseResponse = serializers.deserialize(
            json.decode(response.data),
            specifiedType: FullType(UserModel));
        print("facebookLogin status : ${baseResponse}");
        if (baseResponse != null) {
          return baseResponse;
        } else {
          throw NetworkException();
        }
      } else {
        throw NetworkException();
      }
    } catch (e) {
      print(e.toString());
      throw NetworkException();
    }
  }

}
