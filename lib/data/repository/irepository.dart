import 'dart:io';

import 'package:built_collection/built_collection.dart';
import 'package:loginwithfb/models/user_model/user_model.dart';

abstract class IRepository {

  Future<bool> logout();
  Future<UserModel> facebookLogin(
      {String email, String name, String token, id, String image});
}
