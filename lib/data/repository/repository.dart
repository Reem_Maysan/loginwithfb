import 'dart:io';
import 'dart:math';

import 'package:loginwithfb/data/db_helper/Idb_helper.dart';
import 'package:loginwithfb/data/http_helper/Ihttp_helper.dart';
import 'package:loginwithfb/data/prefs_helper/iprefs_helper.dart';
import 'package:loginwithfb/models/user_model/user_model.dart';

import 'irepository.dart';

class Repository implements IRepository {
  IHttpHelper _ihttpHelper;
  IPrefsHelper _iprefHelper;
  IDbHelper _iDbHelper;

  Repository(this._ihttpHelper, this._iprefHelper, this._iDbHelper);

  @override
  Future<UserModel> facebookLogin(
      {String email, String name, String token, id, String image}) async {
    final user = await _ihttpHelper.facebookLogin(
        email: email, name: name, token: token, id: id, image: image);
    //   final save = await _iprefHelper.saveUser(user);
    return user;
  }

  @override
  Future<bool> logout() async {
    final logout = await _ihttpHelper.logout();
    // final appLogout = await _iprefHelper.logout();
    return logout;
  }
}
